
##### 1. PHENOTYPIC TRUNCATION SELECTION ######

library(plyr)
library(ggplot2)

nReps=10

crossCombs <- readRDS(file="SorghumSudan_GridCombs.rds")

for(i in 1:nReps){
  assign("A", readRDS(paste0("SorghumSudanGrid_",i,".rds")))
  assign(paste0("gridRes_",i), ldply(A, data.frame))
  rm(A)
}

gridRes <- gridRes_1
rm(gridRes_1)
for(t in 2:nReps){
  gridRes <- rbind(gridRes, get(paste0("gridRes_",t)))     # get() makes a character refer to the respective object
  rm(list=paste0("gridRes_",t))
}

head(gridRes)

gridRes = ddply(gridRes, c("year","scenario"),                     
           summarise, 
           gainF1 = mean(mean_F1),
           genVarF1 = mean(var_F1),
           genicVarF1 = mean(genic_F1),
           
           gainF2 = mean(mean_F2),
           gainF2_SE = sd(mean_F2)/sqrt(nReps),
           genVarF2 = mean(var_F2),
           genicVarF2 = mean(genic_F2),
           
           gainF8 = mean(mean_F8),
           genVarF8 = mean(var_F8),
           genicVarF8 = mean(genic_F8))


# save.image("icrisatSorghumSudan_CrossOptimisation") save data to load and plot in R markdown

finalYear <- gridRes[gridRes$year==max(gridRes$year),] # extract data from final year of simulation

range(finalYear$gainF2) # range of mean realised gain (F2) of the different cross combinations
rankFinalYear <- finalYear[order(finalYear$gainF2, decreasing = TRUE),]
head(rankFinalYear)
tail(rankFinalYear)

rankFinalYear$scenario


# extract the top and bottom scenarios to show and plot 

noScn = 5 # number of top and bottom scenarios
best <- data.frame(rankFinalYear$scenario[1:noScn], rankFinalYear$gainF2[1:noScn])
worst <- data.frame(rankFinalYear$scenario[(nrow(rankFinalYear)-(noScn-1)):nrow(rankFinalYear)],
                    rankFinalYear$gainF2[(nrow(rankFinalYear)-(noScn-1)):nrow(rankFinalYear)])

colnames(best) = colnames(worst) = c("Scenario", "gainF2")

bestCombs <- merge(best,crossCombs, by="Scenario", all=FALSE)
bestCombs <- bestCombs[order(bestCombs$gainF2, decreasing = TRUE),]

worstCombs <- merge(worst,crossCombs, by="Scenario", all=FALSE)
worstCombs <- worstCombs[order(worstCombs$gainF2, decreasing = FALSE),]


# show crossCombs of best and worst
bw <- rbind(bestCombs,worstCombs) # list of best and worst scenarios
bwDF <- gridRes[gridRes$scenario%in%bw$Scenario,]


# plot best and worst performing scenarios. 
# several optional plotting characteristics are deactivated
pl <- ggplot(data=bwDF) +
  geom_line(mapping = aes(x=year, y=gainF2, colour=scenario), size=0.7) + 
  #geom_ribbon(aes(x=year,ymin=gainF2-gainF2_SE,ymax=gainF2+gainF2_SE,
  #                fill=scenario),alpha=0.2,linetype=0)+
  #scale_color_manual(values = cls) +
  #scale_linetype_manual(values=lns) +
  #scale_fill_manual(values= cls) +
  theme_bw(base_size = 12) +
  #ylim(yGain) +
  #ggtitle(paste0("d/a = ", as.numeric(domDeg))) +
  #theme(legend.position="none") +
  xlab("") + ylab("") +
  theme(axis.text=element_text(size=14))

pl




