library(AlphaSimR)

## Simulation parameters ----
# Models a single trait representing dry yield

nParentsBrnIn = 15    # number of crossing parents selected each year during burnIn
nCrossesBrnIn = 30    # number of crosses per year
nProgenyBrnIn = 200   # number of individuals per cross

# Number of simulation replications
nReps=10

# Effective population size
Ne = 30

# Genetic mean
genMean = 20

# Total genetic variance
varGen = 10

# Genotype-by-year interaction variance
varGxY = 20 

# Residual error for plots
plotError = 4 # H2 = 0.2 (= 1 / (1+4))

# Residual error for plants
plantError = 99 # H2 = 0.01, low so that family selection has reasonable accuracy

# Number of chromosomes
nChr = 20

# Number of QTL per chromosome
nQtl = 300 # 20 chr x 300 QTL = 6000 alleles with an effect on your quantitative trait!

# Number of SNP per chromosome
#nSnp = 300 # SNP chip with 6000 markers

# Loop over replicates
# Each replicate models 20 years of breeding program and saves the output

yearsBurnin = 20  # years of burnIn breeding 
yearFuture = 30   # years of future breeding after burnIn
yearsTotal = yearsBurnin+yearFuture   # total years of breeding 

for(REP in 1:nReps){
  # Create data.frame for results
  output = data.frame(year = 1:yearsTotal,
                      rep = rep(REP, yearsTotal),
                      scenario = rep("BURNIN", yearsTotal),
                      mean_F1 = numeric(yearsTotal),
                      var_F1 = numeric(yearsTotal),
                      genic_F1 = numeric(yearsTotal),
                      mean_F2 = numeric(yearsTotal),
                      var_F2 = numeric(yearsTotal),
                      genic_F2 = numeric(yearsTotal),
                      mean_F8 = numeric(yearsTotal),
                      var_F8 = numeric(yearsTotal),
                      genic_F8 = numeric(yearsTotal),
                      stringsAsFactors=FALSE)
  
  ## Create founder population ----
  # Represents initial set of parents (species "GENERIC")
  founderPop = runMacs2(nInd = nParentsBrnIn,
                        nChr = nChr,
                        segSites = nQtl, #nQtl+nSnp if SNP array is simulated
                        Ne = Ne)
  
  ## Set simulation parameters ----
  # Add characteristics to the simulated population called "founderPop"
  
  SP = SimParam$new(founderPop)
  
  SP$addTraitAG(nQtlPerChr = nQtl, 
                 mean = genMean, 
                 var = varGen, 
                 varGxE = varGxY)
  
  # add SNP chip 
  #SP$addSnpChip(nSnpPerChr = nSnp)
  
  # turn "founderPop" with all its characteristics 
  # into an actual population object that can be used for simulation
  Parents = newPop(founderPop) 
  
  
  ## 1. FILL THE BREEDING PROGRAM PIPELINE ----
  
  # Presample year p-value for genotype-by-year interactions
  # These values are only used for filling the pipeline
  P = runif(5)
  for(cycle in 1:9){
    
    # Crossing block - season 1 (yr1)
    F1 = randCross(Parents, 
                   nCrosses = nCrossesBrnIn, 
                   nProgeny = 1)
    
    # Grow F1 - season 2 (yr1)
    if(cycle<9){
      SN = F1
    }
    
    # F2 - season 3 (yr2)
    if(cycle<8){
      F2 =self(SN, nProgeny = nProgenyBrnIn)
    }
    
    # F3 - season 4 (yr2)
    if(cycle<7){
      F3 = self(F2)
    }
    
    # F4 - season 5 (yr3)
    if(cycle<6){
      F4 = self(F3)
      F4 = setPheno(F4,
                    varE = plantError,
                    reps = 1, # 1 plant
                    p = P[1])
    }
    
    # F5 - season 6 (yr3)
    if(cycle<5){
      F5 = selectInd(F4, 1200)
      F5 = self(F5)
      F5 = setPheno(F5,
                    varE = plotError,
                    reps = 3, # 3 locs, 1 reps, 1x plot size
                    p = P[2])
    }
    
    # F6: Preliminary Yield Trials - season 7+8 (yr4) 
    if(cycle<4){
      F6_PYT = self(F5) # no selection, still 1200 genotypes
      F6_PYT = setPheno(F6_PYT,
                        varE = plotError,
                        reps = 3*2*2, # 3 locs, 1 reps, 1x plot size
                        p = P[3])
    }
    
    # F7: Advanced Yield Trials - season 9+10 (yr5)
    if(cycle<3){
      F7_AYT = selectInd(F6_PYT, 120)
      F7_AYT = self(F7_AYT)
      F7_AYT = setPheno(F7_AYT,
                        varE = plotError,
                        reps = 6*3*4, # 6 locs, 3 reps, 4x plot size
                        p = P[4])
    }
    
    # F8: On-Farm Trials - season 11+12 (yr6)
    if(cycle<2){
      F8_onFarm = selectInd(F7_AYT, 120)
      F8_onFarm = self(F8_onFarm)
      F8_onFarm = setPheno(F8_onFarm,
                           varE = plotError,
                           reps = 20*2*6, # 20 locs, 2 reps, 6x plot size
                           p = P[5])
    }
  }
  
  
  # Replace presampled p-values for next 40 years
  # These are used in the burn-in and evaluation steps
  P = runif(yearsTotal)
  trainPop <- vector()
  
  ## Run burnIn breeding program ----
  for(year in 1:yearsBurnin){
    
    # select new parents in current year
    Parents = selectInd(F7_AYT, nParentsBrnIn)
    
    # F8: On-Farm Trials - season 11+12 (yr6)
      F8_onFarm = selectInd(F7_AYT, 12)
      F8_onFarm = self(F8_onFarm)
      F8_onFarm = setPheno(F8_onFarm,
                           varE = plotError,
                           reps = 20*2*6, # 20 locs, 2 reps, 6x plot size
                           p = P[year])
    
      
    # F7: Advanced Yield Trials - season 9+10 (yr5)
      F7_AYT = selectInd(F6_PYT, 120)
      F7_AYT = self(F7_AYT)
      F7_AYT = setPheno(F7_AYT,
                        varE = plotError,
                        reps = 6*3*4, # 6 locs, 3 reps, 4x plot size
                        p = P[year])
    
      
    # F6: Preliminary Yield Trials - season 7+8 (yr4) 
      F6_PYT = self(F5) # no selection, still 1200 genotypes
      F6_PYT = setPheno(F6_PYT,
                        varE = plotError,
                        reps = 3*2*2, # 3 locs, 1 reps, 1x plot size
                        p = P[year])
    
      # F5 - season 6 (yr3)
      F5 = selectInd(F4, 1200)
      F5 = self(F5)
      F5 = setPheno(F5,
                    varE = plotError,
                    reps = 3, # 3 locs, 1 reps, 1x plot size
                    p = P[year])
    
      
      # F4 - season 5 (yr3)
      F4 = self(F3)
      F4 = setPheno(F4,
                    varE = plantError,
                    reps = 1, # 1 plant
                    p = P[year])
    
      # F3 - season 4 (yr2)
      F3 = self(F2)
    
      # F2 - season 3 (yr2)
      F2 = self(SN, nProgeny = nProgenyBrnIn)
    
      # Grow F1 - season 2 (yr1)
      SN = F1
    
      # cross parents
      F1 = randCross(Parents, 
                     nCrosses = nCrossesBrnIn, 
                     nProgeny = 1)
    
    
      
    # Report performance of new F1s
    output$mean_F1[year] = meanG(F1)
    output$var_F1[year] = varG(F1)
    output$genic_F1[year] = genicVarG(F1)
    
    # Report performance of new F2s
    output$mean_F2[year] = meanG(F2)
    output$var_F2[year] = varG(F2)
    output$genic_F2[year] = genicVarG(F2)
    
    # Report performance of new F8s
    output$mean_F8[year] = meanG(F8_onFarm)
    output$var_F8[year] = varG(F8_onFarm)
    output$genic_F8[year] = genicVarG(F8_onFarm)
    
  }
  
  # Save output
  cat(paste0("saving REP ", REP, "\n"))
  save.image(paste0("BURNIN_",REP,".RData"))
}

rm(list=ls())
